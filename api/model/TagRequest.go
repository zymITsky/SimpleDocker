package model

type TagRequest struct {
	Source string `json:"source"`
	Tag    string `json:"tag"`
}
